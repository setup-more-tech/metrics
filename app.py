from flask import Flask, request, jsonify
import json
import requests
from flask_cors import CORS
app = Flask(__name__)


@app.route('/api/getMetrics', methods=['POST'])
def getMetrics():
    res = requests.post('http://localhost:8086/getMetrics', json=request.get_json())
    return res.json()

if __name__ == "__main__":
    app.run(debug=True, port = 5000)